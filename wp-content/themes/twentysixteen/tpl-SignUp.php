<?php
/*
Template Name: SignUP EC
Author: Ernesto Quisbert Trujillo
*/
get_header(); 
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
        <!-------------------------- Gaea21 CODE ------------------------------>                        
	  <form method = "post" enctype= "multipart/form-data" action = "<?php echo get_home_url(); ?>/index.php/accueil">
            Prenom*:	<input type = "text" name = "prenom"/>
            </br>Nom*: <input type = "text" name = "nom"/>
            </br>Email*: <input type = "text" name = "email"/>
            </br>Mot de Passe*: <input type = "password" name = "password"/>
            </br>Confirme Mot de Passe*: <input type = "password" name = "password2"/>
            </br></br>Commune*: <select name = "localisation">
			<option value = "0,0,0">-veuillez choisir votre communauté à laquelle vous appartenez-</option>
			<?php							
                $Localisation = $wpdb->get_results("Select DISTINCT a.ID_Pays, a.Nom_Pays, 
                                                        b.ID_Region, b.Nom_Region, 
                                                        c.ID_Ville, c.Nom_Ville
                                                        FROM SYS_Pays a, SYS_Region b, SYS_Ville c
                                                        where c.FK_Region = b.ID_Region
                                                        and c.FK_Pays = a.ID_Pays
                                                        and b.FK_Pays = a.ID_Pays;");
                foreach ($Localisation as $Lieux)
                {
                    echo ('<option value = "'.$Lieux->ID_Ville.','.$Lieux->ID_Region.','.$Lieux->ID_Pays.'">'.$Lieux->Nom_Pays.'-'.$Lieux->Nom_Region.'-'.$Lieux->Nom_Ville.'</option>');
                }
            ?>
            </select>
            </br></br>Avatar: <input name = "avatar" type = "file" id = "avatar">
			</br></br><input type = "submit" value = "creer ma compte!" />
            <input type = "hidden" name = "flag" value = "signUP" />
        </form>
		<!--------------------------------------------------------------------->
	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>