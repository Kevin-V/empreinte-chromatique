<?php
/*
Template Name: Accueil EC
Author: Ernesto Quisbert Trujillo
*/
get_header();
session_start();

$_SESSION['prenom']=$_REQUEST['prenom'];
$Prenom = $_SESSION['prenom'];

$_SESSION['nom']=$_REQUEST['nom'];
$Nom = $_SESSION['nom'];

$_SESSION['email']=$_REQUEST['email'];
$Email = $_SESSION['email'];

$_SESSION['password']=$_REQUEST['password'];
$Password = $_SESSION['password'];
$Password2 = $_POST['password2'];
$PassEncrypted = md5($Password);

$_SESSION['localisation']=$_REQUEST['localisation'];
$Lieux = $_SESSION['localisation'];

$VilleRegionPays = explode(",", $Lieux);
$Ville = $VilleRegionPays[0];
$Region = $VilleRegionPays[1];
$Pays = $VilleRegionPays[2];

$_SESSION['flag']=$_REQUEST['flag'];
$flag = $_SESSION['flag'];





//----------------------------------- INVITATIONS AMIS --------------------------//

$Emails_Amis = $_SESSION['Emails_Amis'];

$donnee=explode(',',$Emails_Amis);
for($i=0; $i<count($donnee);$i++ ) {
	$EmailForEachAmi = $donnee[$i];

	//”””””””””” Requête pour obtenir l'identifiant des amis (invitation par mail)””””””””//
	$idUserAmi = $wpdb->get_results("SELECT *
									FROM EC_Utilisateur
                                    WHERE Email_Utilisateur = '" . $EmailForEachAmi . "'");

	foreach ($idUserAmi as $idAmis) {
		$IdentifiantsAmis = $idAmis->ID_Utilisateur;

		$ID_Amis = $idAmis->ID_Utilisateur;
		//echo $idAmis->ID_Utilisateur . "<br>";

		//var_dump($idUserAmi);

	}

	//”””””””””” Requête pour obtenir l'identifiant de l'utilisateur connecté””””””””//
	$idUserConnecte = $wpdb->get_results("SELECT ID_Utilisateur
									FROM EC_Utilisateur
                                    WHERE Email_Utilisateur = '" . $Email . "'");

	foreach ($idUserConnecte as $idUserCo) {

		$ID_Utilisateur_Connecte = $idUserCo->ID_Utilisateur;
		//echo $idUserCo->ID_Utilisateur . "<br>";

	}





	if (!empty ($ID_Amis)) {
		//”””””””””” Requête pour voir si le lien entre amis est déja effecuté””””””””//
		$idsTablesAmis = $wpdb->get_results("SELECT *
									FROM EC_Amis
                                    WHERE FK_Utilisateur = '" . $ID_Utilisateur_Connecte . "'
                                    AND ID_Amis = '" . $ID_Amis . "' ");

		foreach ($idsTablesAmis as $IDsTablesAMis) {

			$ID_FK_Utilisateur = $IDsTablesAMis->FK_Utilisateur;
			$ID_ID_Amis = $IDsTablesAMis->ID_Amis;
			//echo $idUserCo->ID_Utilisateur . "<br>";

		}

		//var_dump($idsTablesAmis);


		if ($ID_Utilisateur_Connecte != $ID_FK_Utilisateur && $ID_Amis!= $ID_ID_Amis) {

			//”””””””””” Insertion du lien entre amis ””””””””//
			$InsertUtilisateur = "INSERT INTO EC_Amis
		  						(FK_Utilisateur,ID_Amis)
			   					VALUES ('".$ID_Utilisateur_Connecte."','".$ID_Amis."')";
			$wpdb->query($InsertUtilisateur);

			//var_dump($InsertUtilisateur);
		}
		else{
			echo "Lien entre amis déjà effectué";

		}
	}
}


//----------------------------------- AVATAR --------------------------//				  
$titulo = $_POST['prenom'];
$nombre_file = mktime() .'.jpg';
$posicion = 0;

$origen = $_FILES['avatar']['tmp_name'];
//----------------------------------Validations---------------------------------------//
if($Prenom == "" or $Nom == "" or $Email == "" or $Password == "" or $Password2 == "" or $Lieux== "0,0,0")
{
	$Errors[] = "Vous n'avez pas composé tous les champs obligatoires d'inscription";
}

if($Password != $Password2)
{
	$Errors[] = "Les mots de passe ne coincident pas";
}

if(!filter_var($Email,FILTER_VALIDATE_EMAIL ))
{
	$Errors[] = "L'adresse email n'est pas valide";
}
//-------------------------------------------------------------------------------------// 

//------------------------VAriables auxiliaires------------------------------------------//
$SelectUser = "SELECT a.ID_Utilisateur, a.Prenom_Utilisateur
		  FROM EC_Utilisateur a
		  WHERE a.Email_Utilisateur = '".$Email."' 
		  AND a.Password_Utilisateur = '".$PassEncrypted."';";


$registryUser = $wpdb->get_results( $SelectUser, ARRAY_A );
$IDUser = $registryUser[0]["ID_Utilisateur"];
$PrenomUser = $registryUser[0]["Prenom_Utilisateur"];
//--------------------------------------------------------------------------------------//
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
		<!--------------------------Gaea21 Code----------------------------------------->
		</br>
		<h1>PAGE D'ACCUEIL</h1>
		</br>
		<?php
		if(empty($Errors))
		{
			if ($$origen = $_FILES['avatar']['tmp_name'] != "")
			{
				global $wpdb;
				$InsertUtilisateur = "INSERT INTO EC_Utilisateur
		  						(FK_Ville,FK_Region,FK_Pays, Prenom_Utilisateur,Nom_Utilisateur,Email_Utilisateur,Password_Utilisateur,Photo_Utilisateur) 
			   					VALUES ('".$Ville."','".$Region."','".$Pays."',
										'".$Prenom."','".$Nom."','".$Email."',
										MD5('".$Password."'),'".$nombre_file."');";
				$wpdb->query($InsertUtilisateur);
				$origen = $_FILES['avatar']['tmp_name'];
				$destino = "http://www.gaea21.org/empreinte-chromatique/wp-content/themes/twentysixteen/Avatars/$nombre_file";
				move_uploaded_file ($origen, $destino);
			}
			else
			{
				$InsertUtilisateur = "INSERT INTO EC_Utilisateur 
		  						(FK_Ville,FK_Region,FK_Pays, Prenom_Utilisateur,Nom_Utilisateur,Email_Utilisateur,Password_Utilisateur,Photo_Utilisateur) 
			   					VALUES ('".$Ville."','".$Region."','".$Pays."',
										'".$Prenom."','".$Nom."','".$Email."',
										MD5('".$Password."'),'Default.jpg');";
				$wpdb->query($InsertUtilisateur);
			}
			//------------------------VAriables auxiliaires------------------------------------------//
			$SelectUser = "SELECT a.ID_Utilisateur, a.Prenom_Utilisateur, a.Photo_Utilisateur
		  FROM EC_Utilisateur a
		  WHERE a.Email_Utilisateur = '".$Email."' 
		  AND a.Password_Utilisateur = '".$PassEncrypted."';";

			$registryUser = $wpdb->get_results( $SelectUser, ARRAY_A );
			$IDUser = $registryUser[0]["ID_Utilisateur"];
			$PrenomUser = $registryUser[0]["Prenom_Utilisateur"];
			$Photo = $registryUser[0]["Photo_Utilisateur"];
			//--------------------------------------------------------------------------------------//



			echo ('<img src= "'.get_bloginfo('template_directory').'/Avatars/'.$Photo.'" img width= "50" img height= "50" />');
			echo ('<h4>Bonjour '.$PrenomUser.' <a href = "http://www.gaea21.org/empreinte-chromatique/wp-content/themes/twentysixteen/prueba2.php">Connaissez votre empreinte ecologique!</a></h4>');


			echo ('<form method="post" action ="http://www.gaea21.org/empreinte-chromatique/wp-content/themes/twentysixteen/invitation_amis_2.php" onsubmit="return validation();">
						</br>Adresses Emails: <input type ="textarea" name="amis"/>
						<input type="submit" value="Inviter!" />
				 </form>');



			echo ('</br></br>ou</br>Est-ce que vous voulez <a href ="'.get_home_url().'">se deconnecter?</a></h4>');
		}

		else
		{
			if ($flag == "signUP")
			{
				if (!empty($Errors))
				{
					foreach($Errors as $error)
					{
						echo ('- '.$error.'</br>');
					}
					echo ('</br>');
					echo ('<h5><a href = "'.get_home_url().'/index.php/inscription">Retourner au SignUp</a></h5></br>');
				}

			}
			else
			{
				if ($IDUser == NULL)
				{
					echo ('<h5>Désolé, ladresse email ou le mot de passe sont incorrect. Retourner au <a href = "'.get_home_url().'/index.php/login">Login</a></h5></br>');
				}
				else
				{
					echo ('</br>');
					echo ('<h4>Bonjour '.$PrenomUser.', <a href = "http://www.gaea21.org/empreinte-chromatique/wp-content/themes/twentysixteen/prueba2.php">Connaissez votre empreinte ecologique!</a></h4>');



					echo ('</br>ou</br>Est-ce que vous voulez <a href ="'.get_home_url().'">se deconnecter?</a></h4>');
				}
			}
		}

		?>
		<!----------------------------------------------------------------------------------------->
	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
